from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def get_api_url(self):
        return reverse("api_vin", kwargs={"vin": self.vin})



class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.id})


class Appointment(models.Model):
    STATUS_CHOICES = (
        ('canceled', 'canceled'),
        ('finished','finished'),
        ('created','created'),
    )
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES)
    vin = models.CharField(max_length=17)
    customer= models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE,related_name="appointments",)

    def cancel(self):
        self.status = 'canceled'
        self.save()
    
    def create(self):
        self.status = 'created'
        self.save()

    def finish(self):
        self.status = 'finished'
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.id})
