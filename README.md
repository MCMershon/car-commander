# CarCar

This is a project plan for a car dealership application called CarCar. The project involves building two microservices: Service microservice and Sales microservice. The Service microservice will handle appointments, technicians, and automobiles, while the Sales microservice will handle sales,, manufacturers, and customers. The project plan includes instructions for running the project, value objects, models for the Service microservice, and RESTful APIs for both microservices.

# Team

### Person 1 - Gurjinder Singh

Worked on the Service microservice (full stack) and the Inventory microservice (frontend) for the following components:

- VehicleModelForm
- VehicleModelList
- AutomobileForm


### Person 2 - Micah Mershon

Worked on the Sales microservice (full stack) and the Inventory microservice (frontend) for the following components:

- ManufacturerForm
- ManufacturerList
- AutomobileList


# Design

![Design](Design.png)

# Running the Project

### Requirements:

* Git: Version 2.28 or later.
* Docker: Version 20.10 or later.
* Gitlab Repository: [Project Beta](https://gitlab.com/gurjinder124/project-beta)

### Instructions:

1. Open a terminal window.
2. Clone the project repository by running the following command: `git clone https://gitlab.com/gurjinder124/project-beta`
3. Navigate to the cloned repository directory by running the following command: `cd project-beta`
4. Create a Docker volume for the project data by running the following command: `docker volume create beta-data`
5. Build the Docker containers by running the following command: `docker-compose build`
6. Start the Docker containers by running the following command: `docker-compose up`
7. Once the containers are up and running, open a web browser (such as Google Chrome) and navigate to `http://localhost:3000`

<details>
<summary>After following the steps above, seven containers should be running in Docker, and you should be able to access the web app in your web browser. The containers should include the following services:</summary>

![Docker](Docker.png)
</details>

### Creating a superuser:

To log into the Admin application for any microservice, you'll need to create a superuser:

1. Connect to a running container using the command `docker exec -it «api-container-name» bash`. Make sure not to stop your services.

2. Look for the service with the name "-api-" in it. You can use the command `service list` to list all the running services.

3. Run `python manage.py createsuperuser` at the container command prompt.

4. Fill out the form as you normally would.

5. Open the URL for the microservice that you just created the superuser for:

    - Inventory: http://localhost:8100/admin/
    - Service: http://localhost:8080/admin/
    - Sales: http://localhost:8090/admin/

If you encounter any issues while following these instructions, please refer to the project documentation or contact the project team for assistance.

# Routes Outline - URLs/ports

### Inventory Routes

- **Manufacturers** http://localhost:3000/manufacturers/
    -  A list view of all manufacturer objects
- **Create a manufacturer** http://localhost:3000/manufacturers/create/
    - A form for creating a new and unique manufacturer object
- **Models** http://localhost:3000/models/
    - A list view of all vehicle model objects
- **Create a Model** http://localhost:3000/models/create/
    - A form for creating a new vehicle model object
- **Automobiles** http://localhost:3000/automobiles/
    - A list of all automobile objects
- **Create an Automobile** http://localhost:3000/automobiles/create/
    - A form for creating a new automobile object


### Service Routes

- **Technicians** http://localhost:3000/technicians
    -  A list view of all technician in the database
- **Create a technician** http://localhost:3000/technicians/create
    - A form for creating a new and unique technician
- **Service Appointment** http://localhost:3000/appointments
    - A list view of all the service appointments
- **Create a service appointment** http://localhost:3000/appointments/create
    - A form for creating a new service appointments for autombiles
- **Service History** http://localhost:3000/appointments/history
    - A list view of all the service appointments which can be searched with the VIN number


### Sales Routes

- **Salespeople** http://localhost:3000/salespeople/
    -  A list view of all salesperson objects
- **Add a salesperson** http://localhost:3000/salespeople/create/
    - A form for creating a new salesperson object
- **Customers** http://localhost:3000/customers/
    - A list view of all customer objects
- **Add a customer** http://localhost:3000/customers/create/
    - A form for creating a new salesperson object
- **Sales** http://localhost:3000/sales/
    - A list view of all sale objects
- **Add a sale** http://localhost:3000/sales/create/
    - A form for creating a new sale object
- **Sales History** http://localhost:3000/sales/history/
    - A list view of all sale objects, which can be filtered by salesperson using the dropdown menu 


# Inventory Microservice Models

The Inventory microservice has three models: `Manufacturer`, `VehicleModel`, and `Automobile`.

* The `Manufacturer` model has a unique `name` field to store the name of the manufacturer of the vehicles. 

* The `VehicleModel` model has fields for `name`, `picture_url`, and a foreign key to the `Manufacturer` model. 

* The `Automobile` model has fields for `color`, `year`, `vin`, `sold_status`, and a foreign key to the `VehicleModel` model. The `vin` field is unique to store the vehicle identification number for every automobile in the inventory. 



# Inventory Microservice RESTful API

| Action  | Method   | URL   |
|---|---|---|
| List all manufacturers | GET  | http://localhost:8100/api/manufacturers/  |
| Show a specific manufacturer  | GET  | http://localhost:8100/api/manufacturers/:id/  |
| Create a manufacturer  | POST   | http://localhost:8100/api/manufacturers/ |
| Delete a manufacturer  | DELETE  | http://localhost:8100/api/manufacturers/:id//  |
| Update a manufacturer  | PUT  |  http://localhost:8100/api/manufacturers/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "manufacturers" set to a list of all Manufacturer objects.
</summary>
JSON return body:

```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, and name of the requested Manufacturer object.
</summary>
JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Ford"
}
```
</details>

<details>
<summary markdown="span">POST: Creating a manufacturer requires a name field. Returns all of the values from the request body, plus the id of the created object.
</summary>
JSON request body:

```
{
  "name": "Ford"
}
```

JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Ford"
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a manufacturer requires an href, id, and name field. Returns all of the same fields from the request body.
</summary>
JSON request body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}
```

JSON return body:

```
{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Honda"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List models | GET  | http://localhost:8100/api/models/  |
| Show a specific model | GET  |  http://localhost:8100/api/models/:id/  |
| Create a model | POST   | http://localhost:8100/api/models/ |
| Delete a model | DELETE  |  http://localhost:8100/api/models/:id/  |
| Update a model | PUT |  http://localhost:8100/api/models/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "models" set to a list of all VehicleModel objects.
</summary>
JSON return body:

```
{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Ford"
			}
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, name, picture url, and manufacturer of the requested VehicleModel object.
</summary>
JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a model requires a name, picture url, and manufacturer id.
Returns all of the fields from the request body plus the id of the created model, with the manufacturer id replaced by the appropriate manufacturer object.
</summary>
JSON request body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}
```

JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a model requires either a name or picture url, but can take both. The manufacturer cannot be changed. Returns all of the same fields as the POST request.
</summary>
JSON request body:

```
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg"
}
```

JSON return body:

```
{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Ford"
	}
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all automobiles | GET  | http://localhost:8100/api/automobiles/  |
| Show an specific automobile  | GET  | http://localhost:8100/api/automobiles/:vin/  |
| Create an automobile  | POST   | http://localhost:8100/api/automobiles/ |
| Delete an automobile  | DELETE  |  http://localhost:8100/api/automobiles/:vin/  |
| Update an automobile  | PUT  |  http://localhost:8100/api/automobiles/:vin/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "autos" set to a list of all Automobile objects.
</summary>
JSON return body:

```
{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Ford"
				}
			},
			"sold": false
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object containing the href, id, color, year, vin, and model of the requested Automobile object.
</summary>
JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	},
	"sold": false
}
```
</details>

<details>
<summary markdown="span">POST: Creating an automobile requires a color, year, vin, and model id.
Returns all of the fields from the request body plus the id and href of the created model, with the model id replaced by the appropriate VehicleModel object.
</summary>
JSON request body:

```
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120175",
  "model_id": 1
}
```

JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120175",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Ford"
		}
	},
	"sold": false
}
```
</details>

<details>
<summary markdown="span">PUT: Updating an automobile requires color, year, or sold. An The model and vin cannot be changed. Returns all of the same fields as the POST request.
</summary>
JSON request body:

```
{
  "color": "red",
  "year": 2012,
  "sold": true
}
```

JSON return body:

```
{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Ford"
		}
	},
	"sold": true
}
```
</details>


# Sales Microservice Models

The **Sales** microservice has four models: ```Salesperson```, ```Customer```, ```Sale```, and ```AutomobileVO```. ```AutomobileVO``` is a value object that corresponds to the ```Automobile``` model from the **Inventory** microservice.

```Salesperson``` model: Stores salesperson information in three fields: ```first_name```, ```last_name```, and ```employee_id```. ```employee_id``` is a unique field used to identify ```Salesperson``` objects.

```Customer``` model: Stores customer information in four fields: ```first_name```, ```last_name```, ```address```, and ```phone_number```. ```phone_number``` is a unique field used to identify ```Customer``` objects

```AutomobileVO``` model: Stores automobile information in two fields: ```vin```, and ```sold```. ```vin``` is a unique field used to identify ```AutomobileVO``` objects. ```sold``` is a field used to filter out the automobiles
that are available to sell.

The **Sales** microservice also includes a "poll" function which polls the **Inventory** microservice for ```Automobile``` objects every 60 seconds in order to update or create a corresponding ```AutomobileVO``` object.

```Sale``` model: Stores sale information in four fields: ```price```, ```automobile```, ```salesperson```, and ```customer```. ```automobile```, ```salesperson```, and ```customer``` are all Foreign Keys to the
```AutomobileVO```, ```Salesperson```, and ```Customer``` models respectively. This is because in order to have a sale, an automobile to sell, someone to sell it, and someone who buys it all need to exist beforehand.

# Sales Microservice RESTful API

| Action  | Method   | URL   |
|---|---|---|
| List all salespeople | GET  | http://localhost:8090/api/salespeople/  |
| Show a specific salesperson  | GET  |  http://localhost:8090/api/salespeople/:id/  |
| Create a salesperson  | POST   | http://localhost:8090/api/salespeople/ |
| Delete a salesperson  | DELETE  |  http://localhost:8090/api/salespeople/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "salespeople" set to a list of all Salesperson objects.
</summary>
JSON return body:

```
{
	"salespeople": [
		{
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Thomas",
			"last_name": "Thompson",
			"employee_id": "TT123"
		},
		{
			"href": "/api/salespeople/2/",
			"id": 2,
			"first_name": "Charlie",
			"last_name": "Brown",
			"employee_id": "CB123"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "salesperson" set to the requested Salesperson object.
</summary>
JSON return body:

```
{
	"salesperson": {
		"id": 1,
		"first_name": "Thomas",
		"last_name": "Thompson",
		"employee_id": "TT123"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a salesperson requires a first name, last name and employee id. Returns all of the values from the request body, plus the id of the created object.
</summary>
JSON request body:

```
{
  "first_name": "Charlie",
  "last_name": "Brown",
  "employee_id": "CB123"
}
```

JSON return body:

```
{
	"id": 2,
	"first_name": "Charlie",
	"last_name": "Brown",
	"employee_id": "CB123"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all customers | GET  | http://localhost:8090/api/customers/  |
| Show a specific customer  | GET  |  http://localhost:8090/api/customers/:id/  |
| Create a customer  | POST   | http://localhost:8090/api/customers/ |
| Delete a customer  | DELETE  |  http://localhost:8090/api/customers/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "customers" set to a list of all Customer objects.
</summary>
JSON return body:

```
{
	"customers": [
		{
			"href": "/api/customers/1/",
			"id": 1,
			"first_name": "Jimmy",
			"last_name": "John",
			"address": "1234 Evergreen Drive, Seattle, WA",
			"phone_number": "123-456-7890"
		},
		{
			"href": "/api/customers/2/",
			"id": 2,
			"first_name": "Daffy",
			"last_name": "Duck",
			"address": "1234 Story Lane, Seattle, WA",
			"phone_number": "234-567-8901"
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "customer" set to the requested Customer object.
</summary>
JSON return body:

```
{
	"customer": {
		"id": 1,
		"first_name": "Jimmy",
		"last_name": "John",
		"address": "1234 Evergreen Drive, Seattle, WA",
		"phone_number": "123-456-7890"
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a customer requires a first name, last name, address, and phone number. Returns all of the values from the request body, plus the href location and id of the created object.
</summary>
JSON request body:

```
{
	"first_name": "Daffy",
	"last_name": "Duck",
	"address": "1234 Story Lane, Seattle, WA",
	"phone_number": "234-567-8901"
}
```

JSON return body:

```
{
	"href": "/api/customers/2/",
	"id": 2,
	"first_name": "Daffy",
	"last_name": "Duck",
	"address": "1234 Story Lane, Seattle, WA",
	"phone_number": "234-567-8901"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all sales | GET  | http://localhost:8090/api/sales/  |
| Show a specific sale  | GET  |  http://localhost:8090/api/sales/:id/  |
| Create a sale  | POST   | http://localhost:8090/api/sales/ |
| Delete a sale  | DELETE  |  http://localhost:8090/api/sales/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "sales" set to a list of all Sale objects.
</summary>
JSON return body:

```
{
	"sales": [
		{
			"href": "/api/sales/1/",
			"id": 1,
			"price": "111.11",
			"automobile": {
				"id": 1,
				"vin": "283926SJDLD11FF",
				"sold": true
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Thomas",
				"last_name": "Thompson",
				"employee_id": "DFS3AD88"
			},
			"customer": {
				"href": "/api/customers/3/",
				"id": 3,
				"first_name": "Jonah",
				"last_name": "Hill",
				"address": "1234 Evergreen Drive, Seattle, WA",
				"phone_number": "9072537777"
			}
		},
		{
			"href": "/api/sales/2/",
			"id": 2,
			"price": "222.22",
			"automobile": {
				"id": 2,
				"vin": "1C3CC5FB2AN120174",
				"sold": true
			},
			"salesperson": {
				"href": "/api/salespeople/1/",
				"id": 1,
				"first_name": "Thomas",
				"last_name": "Thompson",
				"employee_id": "DFS3AD88"
			},
			"customer": {
				"href": "/api/customers/3/",
				"id": 3,
				"first_name": "Jonah",
				"last_name": "Hill",
				"address": "1234 Evergreen Drive, Seattle, WA",
				"phone_number": "9072537777"
			}
		}
	]
}
```
</details>

<details>
<summary markdown="span">GET: Returns an object with one key "sale" set to the requested Sale object.
</summary>
JSON return body:

```
{
	"sale": {
		"href": "/api/sales/1/",
		"id": 1,
		"price": "111.11",
		"automobile": {
			"id": 2,
			"vin": "283926SJDLD11FF"
		},
		"salesperson": {
			"href": "/api/salespeople/1/",
			"id": 1,
			"first_name": "Thomas",
			"last_name": "Thompson",
			"employee_id": "DFS3AD88"
		},
		"customer": {
			"href": "/api/customers/3/",
			"id": 3,
			"first_name": "Jonah",
			"last_name": "Hill",
			"address": "1234 Evergreen Drive, Seattle, WA",
			"phone_number": "9072537777"
		}
	}
}
```
</details>

<details>
<summary markdown="span">POST: Creating a sale requires a automobile vin, salesperson employee id, and customer phone number. Returns all of the corresponding objects, as well as the href and id of the Sale object.
</summary>
JSON request body:

```
{
	"price": 52222,
	"automobile": "1C3CC5FB2AN120174",
	"salesperson": "DFS3AD88",
	"customer": "9072537777"
}
```

JSON return body:

```
{
	"href": "/api/sales/2/",
	"id": 2,
	"price": 52222,
	"automobile": {
		"id": 2,
		"vin": "1C3CC5FB2AN120174"
	},
	"salesperson": {
		"href": "/api/salespeople/1/",
		"id": 1,
		"first_name": "Thomas",
		"last_name": "Thompson",
		"employee_id": "DFS3AD88"
	},
	"customer": {
		"href": "/api/customers/3/",
		"id": 3,
		"first_name": "Jonah",
		"last_name": "Hill",
		"address": "1234 Evergreen Drive, Seattle, WA",
		"phone_number": "9072537777"
	}
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all automobiles | GET  | http://localhost:8090/api/automobiles/  |
| Update (Sell) an automobile | PUT  | http://localhost:8090/api/automobiles/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "automobiles" set to a list of all AutomobileVO objects.
</summary>
JSON return body:

```
{
	"automobiles": [
		{
			"id": 2,
			"vin": "1C3CC5FB2AN120174",
			"sold": true
		},
		{
			"id": 21,
			"vin": "1likaushd",
			"sold": true
		}
	]
}
```
</details>

<details>
<summary markdown="span">PUT: Updating a AutomobileVO object to mark as "sold" as true only requires a vin. Returns corresponding AutomobileVO object. 
</summary>
JSON request body:

```
{
	"id": 2,
	"vin": "1C3CC5FB2AN120174",
	"sold": true
}
```
</details>


# Service Microservice Models

The Service microservice has three models: `AutomobileVO`, `Technician`, and `Appointment`. As mentioned earlier, `AutomobileVO` is a value object which integrates with the `Automobile` model of the **Inventory** microservice. A Poller is used to poll data from the `Automobile` model into the `AutomobileVO` model.

* The `Appointment` model has fields for `date_time`, `reason`, `status`, `vin`, `customer`, and `technician`. Information for the appointments are stored with this model and it has a foreign key to the `Technician` model. This model has the capability to handle status changes for appointments (cancel, create, finish). The `vin` field stores the VIN number of every automobile in the inventory and also allows clients who did not buy the vehicle from the inventory to make appointments.

* The `Technician` model contains fields for `first_name`, `last_name`, and `employee_id`, which is a primary key.

* The `AutomobileVO` model is integrated with the **Inventory** microservice and has a unique `vin` field.

# Service Microservice RESTful API

| Action  | Method   | URL   |
|---|---|---|
| List all VIN | GET  | http://localhost:8080/api/vin/ |

<details>
<summary markdown="span">GET (List): Returns an object with one key "autos" set to a list of all Autos objects.
</summary>
JSON return body:

```
{
	"autos": [
		{
			"id": 1,
			"vin": "1C3CC5FB2AN120174"
		}
	]
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all technicians | GET  | http://localhost:8080/api/technicians/  |
| Create a technician  | POST   |  http://localhost:8080/api/technicians/ |
| Delete a technician  | DELETE  |  http://localhost:8080/api/technicians/:id/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "technicians" set to a list of all Techcnicians objects.
</summary>
JSON return body:

```
{
	"technicians": [
		{
			"id": 1,
			"first_name": "u",
			"last_name": "Singh",
			"employee_id": 6
		}
	]
}
```
</details>

<details>
<summary markdown="span">POST: Creating a technician requires a first_name, last_name, employee_id. Returns all of the corresponding objects, as well as id of the Tehcnician object.
</summary>
JSON request body:

```
{
	"id": 1,
	"first_name": "u",
	"last_name": "Singh",
	"employee_id": "6"
}
```
</details>

| Action  | Method   | URL   |
|---|---|---|
| List all appointments | GET  | http://localhost:8080/api/appointments/  |
| Create an appointment  | POST   | http://localhost:8080/api/appointments/ |
| Delete an appointment  | DELETE  |  http://localhost:8090/api/appointments/:id/  |
| Change appointment status to cancel  | PUT |  http://localhost:8080/api/appointments/:id/cancel/  |
| Change appointment status to finish  | PUT  |  http://localhost:8080/api/appointments/:id/finish/  |

<details>
<summary markdown="span">GET (List): Returns an object with one key "appointments" set to a list of all Appointment objects.
</summary>
JSON return body:

```
{
	"appointments": [
		{
			"id": 1,
			"date_time": "2023-04-07T21:35:00+00:00",
			"reason": "glass damage",
			"status": "created",
			"vin": "1C3CC5FB2AN120174",
			"customer": "John Wick",
			"technician": {
				"id": 1,
				"first_name": "Gurjinder",
				"last_name": "Singh",
				"employee_id": 2
			}
		}
  ]
}
```
</details>

<details>
<summary markdown="span">POST: Creating an appointment requires a date_time,reason,vin,customer,technician. Returns all of the corresponding objects, as well as id of the appointment object.
</summary>
JSON request body:

```
{
	"id": 11,
	"date_time": "2023-04-20T14:39:00.000Z",
	"reason": "broken engine",
	"status": "created",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "Gurjinder",
		"last_name": "Singh",
		"employee_id": 2
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Changing the status of the appointment to canceled. Returns all of the corresponding objects, as well as id of the appointment object with status changed to canceled.
</summary>
JSON request body:

```
{
	"id": 7,
	"date_time": "2023-04-20T14:39:00+00:00",
	"reason": "broken engine",
	"status": "canceled",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "Gurjinder",
		"last_name": "Singh",
		"employee_id": 2
	}
}
```
</details>

<details>
<summary markdown="span">PUT: Changing the status of the appointment to finished. Returns all of the corresponding objects, as well as id of the appointment object with status changed to finished.
</summary>
JSON request body:

```
{
	"id": 7,
	"date_time": "2023-04-20T14:39:00+00:00",
	"reason": "broken engine",
	"status": "canceled",
	"vin": "123abc",
	"customer": "John Wick",
	"technician": {
		"id": 1,
		"first_name": "Gurjinder",
		"last_name": "Singh",
		"employee_id": 2
	}
}
```
</details>


## Thank you for taking the time to read this README!

I hope you found it helpful in understanding the purpose and usage of this project. If you have any questions or feedback, please feel free to reach us. We are always looking to improve and welcome any suggestions for how we can make this project better.

Happy coding! 
