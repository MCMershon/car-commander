from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

from .encoders import (
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
    AutomobileVOEncoder,
)
from .models import AutomobileVO, Salesperson, Customer, Sale


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                {"salespeople": salespeople},
                encoder=SalespersonEncoder,
            )
        except:
            return JsonResponse(
                {"message": "Get list unsuccessful"},
                status=400,
            )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Creation unsuccessful"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            salesperson = Salesperson.objects.get(id=pk)
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
    else:
        try:
            salesperson = Salesperson.objects.get(id=pk)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Creation unsuccessful"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                {"customer": customer},
                encoder=CustomerEncoder,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
    else:
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404
            )


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        for sale in sales:
            sale.price = str(sale.price)
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            try:
                automobile = AutomobileVO.objects.get(vin=content["automobile"])
                content["automobile"] = automobile
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid vin"},
                    status=404,
                )
            try:
                salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
                content["salesperson"] = salesperson
            except Salesperson.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid employee_id"},
                    status=404,
                )
            try:
                customer = Customer.objects.get(phone_number=content["customer"])
                content["customer"] = customer
            except Customer.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid customer phone_number"},
                    status=404,
                )
            sale = Sale.objects.create(**content)
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Creation unsuccessful"},
                status=400,
            )


@require_http_methods(["GET", "DELETE"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            sale = Sale.objects.get(id=pk)
            sale.price = str(sale.price)
            return JsonResponse(
                {"sale": sale},
                encoder=SaleEncoder,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
    else:
        try:
            sale = Sale.objects.get(id=pk)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )


@require_http_methods(["GET", "PUT"])
def api_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["vin"])
            content["sold"] = True
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )
        AutomobileVO.objects.filter(vin=content["vin"]).update(**content)
        automobile = AutomobileVO.objects.get(vin=content["vin"])
        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
