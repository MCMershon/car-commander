import { NavLink } from 'react-router-dom';
import './index.css';

function Nav() {
    const activeLinkStyle = { backgroundColor: '#212529' };

    return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
        <div className="container-fluid">
            <NavLink className="navbar-brand" to="/">CarCar</NavLink>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse flex-column" id="navbarSupportedContent">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0 flex_justify" style={{ flexWrap: 'wrap'}}>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/manufacturers/">Manufacturers</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/manufacturers/create/">Create a Manufacturer</NavLink>
                    </li>
                    {/* <li className="nav-item dropdown">
                        <NavLink className="nav-link dropdown-toggle text-white" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" activeclassname ="active">
                            Manufacturers
                        </NavLink>
                        <ul className='dropdown-menu'>
                            <li><NavLink className="dropdown-item text-black" activestyle={activeLinkStyle} to="/manufacturers/create/" activeclassname ="active">Create a Manufacturer</NavLink></li>
                        </ul>
                    </li> */}
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/models/">Models</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/models/create/">Create a Model</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/automobiles/">Automobiles</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/automobiles/create/">Create an Automobile</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/salespeople/">Salespeople</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/salespeople/create/">Add a Salesperson</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/customers/">Customers</NavLink>
                    </li>
                    <div className="break"></div>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/customers/create/">Add a Customer</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/sales/">Sales</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/sales/create/">Add a Sale</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/sales/history/">Sales History</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/technicians">Technicians</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/technicians/create">Add a Technician</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link active" aria-current="page" to="/appointments">Service Appointments</NavLink>
                    </li>
                    <li className="nav-item">
                    <NavLink className="nav-link active" aria-current="page" to="/appointments/create">Create a Service Appointment</NavLink>
                    </li>
                    <li className="nav-item">
                    <NavLink className="nav-link active" aria-current="page" to="/appointments/history">Service History</NavLink>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    )
}

export default Nav;
