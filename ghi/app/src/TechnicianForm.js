import React, {useState} from 'react';


function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeID, setEmployeedID] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeID

        const TechnicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        const response = await fetch(TechnicianUrl, fetchConfig);
        if (response.ok) {
            const success = document.getElementById('success');
            success.className = 'text-center';
            const failure = document.getElementById('failure');
            failure.className = 'text-center d-none';
            setFirstName('');
            setLastName('');
            setEmployeedID('');
        } else {
            const success = document.getElementById('success');
            success.className = 'text-center d-none'; 
            const failure = document.getElementById('failure');
            failure.className = 'text-center'; 
        }
        
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }
    const handleEmployeeIDChange = (event) => {
        const value = event.target.value;
        setEmployeedID(value);
    }

    return (
        <div className="shadow p-4 mt-4">
            <h1>Add a Technician</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                    <input value={firstName} onChange={handleFirstNameChange} placeholder="firstName" required type="text" name="firstName" id="firstName" className="form-control"/>
                    <label htmlFor="firstName">First Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={lastName} onChange={handleLastNameChange} placeholder="lastName" required type="text" name="lastName" id="lastName" className="form-control"/>
                    <label htmlFor="lastName">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={employeeID} onChange={handleEmployeeIDChange} placeholder="employeeID" required type="number" name="employeeID" id="employeeID" className="form-control"/>
                    <label htmlFor="employeeID">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            <h4 id='success' className='text-center d-none'>Technician successfully created!</h4>
            <h5 id='failure' className='text-center d-none'>Failed to create technician. Please try again.</h5>
        </div>
    );
}

export default TechnicianForm;
